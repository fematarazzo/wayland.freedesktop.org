<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<link href="wayland.css" rel="stylesheet" type="text/css">
<title>Building Weston</title>
</head>

<body>
<h1><a href="index.html"><img src="wayland.png" alt="Wayland logo"></a></h1>

<h2>Building Weston</h2>

<p>
Most major Linux distributions now include releases of Wayland and
Weston in their package management systems.
</p>

<p>
You can also manually build the stack yourself.  The directions below are
provided as a guide, but will need adapted for the Linux system you're using.
The instructions below assume some familiarity with
<a href="http://git-scm.com/">git</a>,
<a href="http://www.freedesktop.org/wiki/Software/pkg-config/">pkg-config</a>,
<a href="https://mesonbuild.com/">meson</a>,
and building and running experimental software.</p>

<p>Wayland is built with <a href="https://mesonbuild.com/">Meson</a>. If
necessary, the latest Meson can be installed as a user with:</p>
<pre>
$ pip3 install --user meson
</pre>

<p><tt>pip3</tt> installs meson in <tt>~/.local/</tt> so you might need to add
<tt>~/.local/bin</tt> to your <tt>PATH</tt> environment variable, if it is not
already added.</p>

<p>Some help for figuring out how to fix dependency problems:
<a href="http://who-t.blogspot.com.au/2014/05/configure-fails-with-no-package-foo.html">Configure
fails with "No package 'foo' found" - and how to fix it</a>.</p>

<h2>Hardware / Drivers</h2>

<p>X output requires <a href="http://dri.freedesktop.org/wiki/">DRI2</a>.
Output outside of X requires <a href="http://dri.freedesktop.org/wiki/DRM/">DRM</a> and <a href="https://wiki.archlinux.org/index.php/kernel_mode_setting">Kernel Mode
Setting (KMS)</a> with the page flip ioctl.  These are supported by all
open-source drivers.</p>

<h2 id="environment">Setting up the environment</h2>
<h3>Installing in a custom location</h3>
<p>If you do not want to install system wide, you'll need to set
the following environment variables to get various libraries to link
appropriately:</p>

<pre>
export WLD=$HOME/install   <span class="comment"># change this to another location if you prefer</span>
export LD_LIBRARY_PATH=$WLD/lib
export PKG_CONFIG_PATH=$WLD/lib/pkgconfig/:$WLD/share/pkgconfig/
export PATH=$WLD/bin:$PATH
</pre>

<p>Do not set LD_LIBRARY_PATH as your default, it will break things.</p>
<p>You may put the above in a script and source it in the terminal
you wish to build the packages.</p>

<h3>Installing system wide</h3>

<p>To install system wide, you'll need to set WLD differently, add
some switches to every meson invocation, and use sudo for the install step.
Choose the libdir as appropriate for your distribution and
architecture (it may be <code>/usr/lib32</code> or <code>/usr/lib64</code>).</p>

<pre>
export WLD=/usr
...
meson build/ --prefix=$WLD --libdir=/usr/lib --sysconfdir=/etc
ninja -C build/
sudo ninja -C build/ install
...
</pre>

<h2>Wayland libraries</h2>

<p>This is required in order to be able to build Mesa with the Wayland EGL platform.</p>

<pre>
$ git clone <a href="https://gitlab.freedesktop.org/wayland/wayland.git">https://gitlab.freedesktop.org/wayland/wayland.git</a>
$ cd wayland
$ meson build/ --prefix=$WLD
$ ninja -C build/ install
$ cd ..
</pre>

<ul>
<li>You can avoid the <a href="http://www.stack.nl/~dimitri/doxygen/">Doxygen</a> dependency
with <code>-Ddocumentation=false</code>.</li>

<li>Under Arch Linux,  <a href="https://wiki.archlinux.org/index.php/Docbook#Setting_up_Docbook_in_Arch">DocBook dependencies</a> will be needed when building documentation. These include <code>docbook-xml</code> and <code>docbook-xsl</code>.</li>

</ul>

<h2>Wayland protocols</h2>

<p>This contains a set of protocols that add functionality not available in the Wayland core protocol.</p>

<pre>
$ git clone <a href="https://gitlab.freedesktop.org/wayland/wayland-protocols.git">https://gitlab.freedesktop.org/wayland/wayland-protocols.git</a>
$ cd wayland-protocols
$ meson build/ --prefix=$WLD
$ ninja -C build/ install
$ cd ..
</pre>

<h2>Mesa</h2>

<p><a href="http://www.mesa3d.org/egl.html">Mesa EGL</a> and Mesa Vulkan stacks
support Wayland. Weston's hardware acceleration (GL-renderer) depends on EGL
GBM platform. Many Wayland applications, including some Weston demos, depend on
EGL Wayland platform.</p>

<p>For building Mesa, refer to the upstream
<a href="https://www.mesa3d.org/install.html">build instructions</a>.
When configuring Mesa to make the most out of Weston, make sure that OpenGL ES 2
and GBM are enabled, and that EGL platforms includes wayland.</p>

<p>If you plan to compile <a href="xserver.html">XWayland</a> you may
want to install any dependencies it needs now. This is so Mesa
uses the same header files as xwayland.
</p>

<h2>libinput</h2>

<p><a href="http://www.freedesktop.org/wiki/Software/libinput/">Libinput</a>
translates evdev events into Wayland events. It has been split from
Weston as the code is reusable by any Wayland compositor on Linux.
Head to
<a href="https://wayland.freedesktop.org/libinput/doc/latest/building_libinput.html">
libinput docs</a> for instructions on how to build it.</p>

<h2>Weston and demo applications</h2>

<p>Weston is the reference implementation of a Wayland compositor.  It's
available in the weston repo and comes with a few demo applications.
</p>

<p>Weston's Meson build does not do autodetection and it defaults to all
features enabled, which means you likely hit missing dependencies on the first
try. If a dependency is avoidable through a build option, the error message
should tell you what option can be used to avoid it. You may need to disable
several features if you want to avoid certain dependencies.</p>

<pre>
$ git clone <a href="https://gitlab.freedesktop.org/wayland/weston.git">https://gitlab.freedesktop.org/wayland/weston.git</a>
$ cd weston
$ meson build/ --prefix=$WLD
$ ninja -C build/ install
$ cd ..
</pre>

<p>The <code>meson</code> command populates the build directory. This step can
fail due to missing dependencies. Any build options you want can be added on
that line, e.g. <code>meson build/ --prefix=$WLD -Dsimple-dmabuf-drm=intel</code>.
All the build options can be found in the file
<a href="https://gitlab.freedesktop.org/wayland/weston/blob/master/meson_options.txt"><code>meson_options.txt</code></a>.</p>

<p>Once the build directory has been successfully populated, you can inspect the
configuration with <code>meson configure build/</code>. If you need to change an
option, you can do e.g.
<code>meson configure build/ -Dsimple-dmabuf-drm=intel</code>.</p>

<h2>Running Weston</h2>

<p>Weston creates its unix socket file (for example, <code>wayland-0</code>)
in the directory specified by the required environment variable
<a href="http://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html"><code>$XDG_RUNTIME_DIR</code></a>. Clients use the same variable to find that
socket. This is provided using systemd by some distributions (Fedora,
<a href="https://www.archlinux.org/news/systemd-tools-replaces-udev/">Arch
since June 2012</a> or Exherbo).
<a href="https://bugs.launchpad.net/ubuntu/+source/pam-xdg-support/+bug/894391">Ubuntu began providing it in Quantal.</a>
It is not provided by Gentoo.
</p>

<p>
If <code>$XDG_RUNTIME_DIR</code> isn't automatically set for you, you can setup
<a href="https://github.com/ifreund/rundird">rundird</a> or
<a href="https://github.com/jjk-jacky/pam_rundir">pam_rundir</a>.

<p>Optionally, copy the <code>weston.ini</code> file and edit it to set a
background image that you like:</p>

<pre>
$ mkdir -p ~/.config
$ cp weston/weston.ini ~/.config
$ $EDITOR ~/.config/weston.ini
</pre>

<p>If <code>$DISPLAY</code> is set, then Weston will run under X in a
window and take input from X. Run the compositor by typing:</p>

<pre>
$ weston
</pre>

<p>Otherwise (outside of X) it will run on the KMS/DRM framebuffer and
take input from evdev devices.</p>

<p>If logind is not available, running Weston directly won't work. A
privileged helper, weston-launch, will be required in this case. It needs to be
setuid-root.</p>

<p>To run clients, the second button in the top bar will run
weston-terminal, from which you can run clients. It is also possible to
run clients from a different VT when running on hardware, or from an
xterm if running under X. There are a few demo clients available in the
weston build directory, but they are all pretty simple and mostly for
testing specific features in the wayland protocol: </p>

<ul>
  <li>'weston-terminal' is a simple terminal emulator, not very compliant at all,
    but works well enough for bash</li>
  <li>'weston-flower' draws a flower on the screen, testing the frame
    protocol</li>
  <li>'weston-gears' glxgears, but for wayland</li>
  <li>'weston-smoke' tests SHM buffer sharing</li>
  <li>'weston-image' loads the image files passed on the command line and
    shows them</li>
  <li>'weston-view' does the same for pdf files</li>
  <li>'weston-resizor' demonstrates smooth window resizing
    (use up and down keys)</li>
  <li>'weston-eventdemo' reports libtoytoolkit's
    events to console (see weston-eventdemo --help)</li>
</ul>

<p>Optional environment variables which will get you more debugging
output:</p>

<pre>
export MESA_DEBUG=1
export EGL_LOG_LEVEL=debug
export LIBGL_DEBUG=verbose
export WAYLAND_DEBUG=1
</pre>

<h2>XWayland</h2>
<p>
<a href="xserver.html">Directions for building support for X clients (XWayland)</a>
</p>

</body>
</html>
