����      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �target���)��}�(h�.. _palm_detection:�h]�h}�(h]�h]�h]�h]�h]��refid��palm-detection�uhhhKhhhhh�:/home/whot/code/libinput/build/doc/user/palm-detection.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Palm detection�h]�h �Text����Palm detection�����}�(hh6hh4hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh/hhhh,hKubh �	paragraph���)��}�(h��Palm detection tries to identify accidental touches while typing, while
using the trackpoint and/or during general use of the touchpad area.�h]�h9��Palm detection tries to identify accidental touches while typing, while
using the trackpoint and/or during general use of the touchpad area.�����}�(hhHhhFhhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubhE)��}�(hX!  On most laptops typing on the keyboard generates accidental touches on the
touchpad with the palm (usually the area below the thumb). This can lead to
cursor jumps or accidental clicks. On large touchpads, the palm may also
touch the bottom edges of the touchpad during normal interaction.�h]�h9X!  On most laptops typing on the keyboard generates accidental touches on the
touchpad with the palm (usually the area below the thumb). This can lead to
cursor jumps or accidental clicks. On large touchpads, the palm may also
touch the bottom edges of the touchpad during normal interaction.�����}�(hhVhhThhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK
hh/hhubhE)��}�(hXX  Interference from a palm depends on the size of the touchpad and the position
of the user's hand. Data from touchpads showed that almost all palm events
during tying on a Lenovo T440 happened in the left-most and right-most 5% of
the touchpad. The T440 series has one of the largest touchpads, other
touchpads are less affected by palm touches.�h]�h9XZ  Interference from a palm depends on the size of the touchpad and the position
of the user’s hand. Data from touchpads showed that almost all palm events
during tying on a Lenovo T440 happened in the left-most and right-most 5% of
the touchpad. The T440 series has one of the largest touchpads, other
touchpads are less affected by palm touches.�����}�(hhdhhbhhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubhE)��}�(h�hlibinput has multiple ways of detecting a palm, each of which depends on
hardware-specific capabilities.�h]�h9�hlibinput has multiple ways of detecting a palm, each of which depends on
hardware-specific capabilities.�����}�(hhrhhphhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�:ref:`palm_tool`�h]�hE)��}�(hh�h]��sphinx.addnodes��pending_xref���)��}�(hh�h]�h �inline���)��}�(hh�h]�h9�	palm_tool�����}�(hhhh�ubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhh�hh�ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�h��refexplicit���	reftarget��	palm_tool��refdoc��palm-detection��refwarn��uhh�hh,hKhh�ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhhh,hNubh�)��}�(h�:ref:`palm_pressure`�h]�hE)��}�(hh�h]�h�)��}�(hh�h]�h�)��}�(hh�h]�h9�palm_pressure�����}�(hhhh�ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hh�ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�hӌrefexplicit��h��palm_pressure�h�h�h��uhh�hh,hKhh�ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhhh,hNubh�)��}�(h�:ref:`palm_touch_size`�h]�hE)��}�(hh�h]�h�)��}�(hh�h]�h�)��}�(hh�h]�h9�palm_touch_size�����}�(hhhh�ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hh�ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j  �refexplicit��h��palm_touch_size�h�h�h��uhh�hh,hKhh�ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhhh,hNubh�)��}�(h�:ref:`palm_exclusion_zones`�h]�hE)��}�(hj!  h]�h�)��}�(hj!  h]�h�)��}�(hj!  h]�h9�palm_exclusion_zones�����}�(hhhj)  ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hj&  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j3  �refexplicit��h��palm_exclusion_zones�h�h�h��uhh�hh,hKhj#  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhj  ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhhh,hNubh�)��}�(h�:ref:`trackpoint-disabling`�h]�hE)��}�(hjQ  h]�h�)��}�(hjQ  h]�h�)��}�(hjQ  h]�h9�trackpoint-disabling�����}�(hhhjY  ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hjV  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�jc  �refexplicit��h��trackpoint-disabling�h�h�h��uhh�hh,hKhjS  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhjO  ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhhh,hNubh�)��}�(h�:ref:`disable-while-typing`�h]�hE)��}�(hj�  h]�h�)��}�(hj�  h]�h�)��}�(hj�  h]�h9�disable-while-typing�����}�(hhhj�  ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��h��disable-while-typing�h�h�h��uhh�hh,hKhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhj  ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhhh,hNubh�)��}�(h� :ref:`stylus-touch-arbitration`
�h]�hE)��}�(h�:ref:`stylus-touch-arbitration`�h]�h�)��}�(hj�  h]�h�)��}�(hj�  h]�h9�stylus-touch-arbitration�����}�(hhhj�  ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��h��stylus-touch-arbitration�h�h�h��uhh�hh,hKhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhhh,hNubeh}�(h]�h]�h]�h]�h]��bullet��-�uhh~hh,hKhh/hhubhE)��}�(h�MPalm detection is always enabled, with the exception of
disable-while-typing.�h]�h9�MPalm detection is always enabled, with the exception of
disable-while-typing.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK hh/hhubh)��}�(h�.. _palm_tool:�h]�h}�(h]�h]�h]�h]�h]�h*�	palm-tool�uhhhK(hh/hhhh,ubh.)��}�(hhh]�(h3)��}�(h�*Palm detection based on firmware labelling�h]�h9�*Palm detection based on firmware labelling�����}�(hj  hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj  hhhh,hK'ubhE)��}�(h��Some devices provide palm detection in the firmware, forwarded by the kernel
as the ``EV_ABS/ABS_MT_TOOL`` axis with a value of ``MT_TOOL_PALM``
(whenever a palm is detected). libinput honors that value and switches that
touch to a palm.�h]�(h9�TSome devices provide palm detection in the firmware, forwarded by the kernel
as the �����}�(h�TSome devices provide palm detection in the firmware, forwarded by the kernel
as the �hj  hhhNhNubh �literal���)��}�(h�``EV_ABS/ABS_MT_TOOL``�h]�h9�EV_ABS/ABS_MT_TOOL�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhj  hj  ubh9� axis with a value of �����}�(h� axis with a value of �hj  hhhNhNubj  )��}�(h�``MT_TOOL_PALM``�h]�h9�MT_TOOL_PALM�����}�(hhhj0  ubah}�(h]�h]�h]�h]�h]�uhj  hj  ubh9�]
(whenever a palm is detected). libinput honors that value and switches that
touch to a palm.�����}�(h�]
(whenever a palm is detected). libinput honors that value and switches that
touch to a palm.�hj  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK)hj  hhubh)��}�(h�.. _palm_pressure:�h]�h}�(h]�h]�h]�h]�h]�h*�palm-pressure�uhhhK3hj  hhhh,ubeh}�(h]�(�*palm-detection-based-on-firmware-labelling�j   eh]�h]�(�*palm detection based on firmware labelling��	palm_tool�eh]�h]�uhh-hh/hhhh,hK'�expect_referenced_by_name�}�jZ  j�  s�expect_referenced_by_id�}�j   j�  subh.)��}�(hhh]�(h3)��}�(h� Palm detection based on pressure�h]�h9� Palm detection based on pressure�����}�(hjf  hjd  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hja  hhhh,hK2ubhE)��}�(hX�  The simplest form of palm detection labels a touch as palm when the pressure
value goes above a certain threshold. This threshold is usually high enough
that it cannot be triggered by a finger movement. One a touch is labelled as
palm based on pressure, it will remain so even if the pressure drops below
the threshold again. This ensures that a palm remains a palm even when the
pressure changes as the user is typing.�h]�h9X�  The simplest form of palm detection labels a touch as palm when the pressure
value goes above a certain threshold. This threshold is usually high enough
that it cannot be triggered by a finger movement. One a touch is labelled as
palm based on pressure, it will remain so even if the pressure drops below
the threshold again. This ensures that a palm remains a palm even when the
pressure changes as the user is typing.�����}�(hjt  hjr  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK4hja  hhubhE)��}�(h�vFor some information on how to detect pressure on a touch and debug the
pressure ranges, see :ref:`touchpad_pressure`.�h]�(h9�]For some information on how to detect pressure on a touch and debug the
pressure ranges, see �����}�(h�]For some information on how to detect pressure on a touch and debug the
pressure ranges, see �hj�  hhhNhNubh�)��}�(h�:ref:`touchpad_pressure`�h]�h�)��}�(hj�  h]�h9�touchpad_pressure�����}�(hhhj�  ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��h��touchpad_pressure�h�h�h��uhh�hh,hK;hj�  ubh9�.�����}�(h�.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK;hja  hhubh)��}�(h�.. _palm_touch_size:�h]�h}�(h]�h]�h]�h]�h]�h*�palm-touch-size�uhhhKChja  hhhh,ubeh}�(h]�(� palm-detection-based-on-pressure�jS  eh]�h]�(� palm detection based on pressure��palm_pressure�eh]�h]�uhh-hh/hhhh,hK2j]  }�j�  jI  sj_  }�jS  jI  subh.)��}�(hhh]�(h3)��}�(h�"Palm detection based on touch size�h]�h9�"Palm detection based on touch size�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hKBubhE)��}�(hX  On touchads that support the ``ABS_MT_TOUCH_MAJOR`` axes, libinput can perform
palm detection based on the size of the touch ellipse. This works similar to
the pressure-based palm detection in that a touch is labelled as palm when
it exceeds the (device-specific) touch size threshold.�h]�(h9�On touchads that support the �����}�(h�On touchads that support the �hj�  hhhNhNubj  )��}�(h�``ABS_MT_TOUCH_MAJOR``�h]�h9�ABS_MT_TOUCH_MAJOR�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj  hj�  ubh9�� axes, libinput can perform
palm detection based on the size of the touch ellipse. This works similar to
the pressure-based palm detection in that a touch is labelled as palm when
it exceeds the (device-specific) touch size threshold.�����}�(h�� axes, libinput can perform
palm detection based on the size of the touch ellipse. This works similar to
the pressure-based palm detection in that a touch is labelled as palm when
it exceeds the (device-specific) touch size threshold.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKDhj�  hhubhE)��}�(h�xFor some information on how to detect the size of a touch and debug the
touch size ranges, see :ref:`touchpad_pressure`.�h]�(h9�_For some information on how to detect the size of a touch and debug the
touch size ranges, see �����}�(h�_For some information on how to detect the size of a touch and debug the
touch size ranges, see �hj�  hhhNhNubh�)��}�(h�:ref:`touchpad_pressure`�h]�h�)��}�(hj  h]�h9�touchpad_pressure�����}�(hhhj  ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hj  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j  �refexplicit��h��touchpad_pressure�h�h�h��uhh�hh,hKIhj�  ubh9�.�����}�(hj�  hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKIhj�  hhubh)��}�(h�.. _palm_exclusion_zones:�h]�h}�(h]�h]�h]�h]�h]�h*�palm-exclusion-zones�uhhhKQhj�  hhhh,ubeh}�(h]�(�"palm-detection-based-on-touch-size�j�  eh]�h]�(�"palm detection based on touch size��palm_touch_size�eh]�h]�uhh-hh/hhhh,hKBj]  }�j=  j�  sj_  }�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�Palm exclusion zones�h]�h9�Palm exclusion zones�����}�(hjG  hjE  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hjB  hhhh,hKPubhE)��}�(hX�  libinput enables palm detection on the left, right and top edges of the
touchpad. Two exclusion zones are defined  on the left and right edge of the
touchpad. If a touch starts in the exclusion zone, it is considered a palm
and the touch point is ignored. However, for fast cursor movements across
the screen, it is common for a finger to start inside an exclusion zone and
move rapidly across the touchpad. libinput detects such movements and avoids
palm detection on such touch sequences.�h]�h9X�  libinput enables palm detection on the left, right and top edges of the
touchpad. Two exclusion zones are defined  on the left and right edge of the
touchpad. If a touch starts in the exclusion zone, it is considered a palm
and the touch point is ignored. However, for fast cursor movements across
the screen, it is common for a finger to start inside an exclusion zone and
move rapidly across the touchpad. libinput detects such movements and avoids
palm detection on such touch sequences.�����}�(hjU  hjS  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKRhjB  hhubhE)��}�(h��Another exclusion zone is defined on the top edge of the touchpad. As with
the edge zones, libinput detects vertical movements out of the edge zone and
avoids palm detection on such touch sequences.�h]�h9��Another exclusion zone is defined on the top edge of the touchpad. As with
the edge zones, libinput detects vertical movements out of the edge zone and
avoids palm detection on such touch sequences.�����}�(hjc  hja  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKZhjB  hhubhE)��}�(h��Each side edge exclusion zone is divided into a top part and a bottom part.
A touch starting in the top part of the exclusion zone does not trigger a
tap (see :ref:`tapping`).�h]�(h9��Each side edge exclusion zone is divided into a top part and a bottom part.
A touch starting in the top part of the exclusion zone does not trigger a
tap (see �����}�(h��Each side edge exclusion zone is divided into a top part and a bottom part.
A touch starting in the top part of the exclusion zone does not trigger a
tap (see �hjo  hhhNhNubh�)��}�(h�:ref:`tapping`�h]�h�)��}�(hjz  h]�h9�tapping�����}�(hhhj|  ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hjx  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��h��tapping�h�h�h��uhh�hh,hK^hjo  ubh9�).�����}�(h�).�hjo  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK^hjB  hhubhE)��}�(h��In the diagram below, the exclusion zones are painted red.
Touch 'A' starts inside the exclusion zone and moves
almost vertically. It is considered a palm and ignored for cursor movement,
despite moving out of the exclusion zone.�h]�h9��In the diagram below, the exclusion zones are painted red.
Touch ‘A’ starts inside the exclusion zone and moves
almost vertically. It is considered a palm and ignored for cursor movement,
despite moving out of the exclusion zone.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKbhjB  hhubhE)��}�(h��Touch 'B' starts inside the exclusion zone but moves horizontally out of the
zone. It is considered a valid touch and controls the cursor.�h]�h9��Touch ‘B’ starts inside the exclusion zone but moves horizontally out of the
zone. It is considered a valid touch and controls the cursor.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKghjB  hhubhE)��}�(hX  Touch 'C' occurs in the top part of the exclusion zone. Despite being a
tapping motion, it does not generate an emulated button event. Touch 'D'
likewise occurs within the exclusion zone but in the bottom half. libinput
will generate a button event for this touch.�h]�h9X  Touch ‘C’ occurs in the top part of the exclusion zone. Despite being a
tapping motion, it does not generate an emulated button event. Touch ‘D’
likewise occurs within the exclusion zone but in the bottom half. libinput
will generate a button event for this touch.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKjhjB  hhubh �figure���)��}�(hhh]�h �image���)��}�(h�2.. figure:: palm-detection.svg
    :align: center
�h]�h}�(h]�h]�h]�h]�h]��uri��palm-detection.svg��
candidates�}��*�j�  suhj�  hj�  hh,hNubah}�(h]�h]�h]�h]�h]��align��center�uhj�  hjB  hhhh,hNubh)��}�(h�.. _trackpoint-disabling:�h]�h}�(h]�h]�h]�h]�h]�h*�trackpoint-disabling�uhhhKwhjB  hhhh,ubeh}�(h]�(j6  �id2�eh]�h]�(�palm exclusion zones��palm_exclusion_zones�eh]�h]�uhh-hh/hhhh,hKPj]  }�j�  j,  sj_  }�j6  j,  subh.)��}�(hhh]�(h3)��}�(h�$Palm detection during trackpoint use�h]�h9�$Palm detection during trackpoint use�����}�(hj  hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hKvubhE)��}�(h��If a device provides a
`trackpoint <http://en.wikipedia.org/wiki/Pointing_stick>`_, it is
usually located above the touchpad. This increases the likelihood of
accidental touches whenever the trackpoint is used.�h]�(h9�If a device provides a
�����}�(h�If a device provides a
�hj  hhhNhNubh �	reference���)��}�(h�;`trackpoint <http://en.wikipedia.org/wiki/Pointing_stick>`_�h]�h9�
trackpoint�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]��name��
trackpoint��refuri��+http://en.wikipedia.org/wiki/Pointing_stick�uhj  hj  ubh)��}�(h�. <http://en.wikipedia.org/wiki/Pointing_stick>�h]�h}�(h]��
trackpoint�ah]�h]��
trackpoint�ah]�h]��refuri�j,  uhh�
referenced�Khj  ubh9��, it is
usually located above the touchpad. This increases the likelihood of
accidental touches whenever the trackpoint is used.�����}�(h��, it is
usually located above the touchpad. This increases the likelihood of
accidental touches whenever the trackpoint is used.�hj  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKxhj�  hhubhE)��}�(hX�  libinput disables the touchpad whenever it detects trackpoint activity for a
certain timeout until after trackpoint activity stops. Touches generated
during this timeout will not move the pointer, and touches started during
this timeout will likewise not move the pointer (allowing for a user to rest
the palm on the touchpad while using the trackstick).
If the touchpad is disabled, the :ref:`top software buttons <t440_support>`
remain enabled.�h]�(h9X�  libinput disables the touchpad whenever it detects trackpoint activity for a
certain timeout until after trackpoint activity stops. Touches generated
during this timeout will not move the pointer, and touches started during
this timeout will likewise not move the pointer (allowing for a user to rest
the palm on the touchpad while using the trackstick).
If the touchpad is disabled, the �����}�(hX�  libinput disables the touchpad whenever it detects trackpoint activity for a
certain timeout until after trackpoint activity stops. Touches generated
during this timeout will not move the pointer, and touches started during
this timeout will likewise not move the pointer (allowing for a user to rest
the palm on the touchpad while using the trackstick).
If the touchpad is disabled, the �hjF  hhhNhNubh�)��}�(h�*:ref:`top software buttons <t440_support>`�h]�h�)��}�(hjQ  h]�h9�top software buttons�����}�(hhhjS  ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hjO  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j]  �refexplicit��h��t440_support�h�h�h��uhh�hh,hK}hjF  ubh9�
remain enabled.�����}�(h�
remain enabled.�hjF  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK}hj�  hhubh)��}�(h�.. _disable-while-typing:�h]�h}�(h]�h]�h]�h]�h]�h*�disable-while-typing�uhhhK�hj�  hhhh,ubeh}�(h]�(�$palm-detection-during-trackpoint-use�j�  eh]�h]�(�$palm detection during trackpoint use��trackpoint-disabling�eh]�h]�uhh-hh/hhhh,hKvj]  }�j�  j�  sj_  }�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�Disable-while-typing�h]�h9�Disable-while-typing�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hK�ubhE)��}�(hX�  libinput automatically disables the touchpad for a timeout after a key
press, a feature traditionally referred to as "disable while typing" and
previously available through the
`syndaemon(1) <http://linux.die.net/man/1/syndaemon>`_ command. libinput does
not require an external command and the feature is currently enabled for all
touchpads but will be reduced in the future to only apply to touchpads where
finger width or pressure data is unreliable.�h]�(h9��libinput automatically disables the touchpad for a timeout after a key
press, a feature traditionally referred to as “disable while typing” and
previously available through the
�����}�(h��libinput automatically disables the touchpad for a timeout after a key
press, a feature traditionally referred to as "disable while typing" and
previously available through the
�hj�  hhhNhNubj  )��}�(h�6`syndaemon(1) <http://linux.die.net/man/1/syndaemon>`_�h]�h9�syndaemon(1)�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��name��syndaemon(1)�j+  �$http://linux.die.net/man/1/syndaemon�uhj  hj�  ubh)��}�(h�' <http://linux.die.net/man/1/syndaemon>�h]�h}�(h]��syndaemon-1�ah]�h]��syndaemon(1)�ah]�h]��refuri�j�  uhhj:  Khj�  ubh9�� command. libinput does
not require an external command and the feature is currently enabled for all
touchpads but will be reduced in the future to only apply to touchpads where
finger width or pressure data is unreliable.�����}�(h�� command. libinput does
not require an external command and the feature is currently enabled for all
touchpads but will be reduced in the future to only apply to touchpads where
finger width or pressure data is unreliable.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubhE)��}�(h�=Notable behaviors of libinput's disable-while-typing feature:�h]�h9�?Notable behaviors of libinput’s disable-while-typing feature:�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubh)��}�(hhh]�(h�)��}�(h��Two different timeouts are used, after a single key press the timeout is
short to ensure responsiveness. After multiple key events, the timeout is
longer to avoid accidental pointer manipulation while typing.�h]�hE)��}�(h��Two different timeouts are used, after a single key press the timeout is
short to ensure responsiveness. After multiple key events, the timeout is
longer to avoid accidental pointer manipulation while typing.�h]�h9��Two different timeouts are used, after a single key press the timeout is
short to ensure responsiveness. After multiple key events, the timeout is
longer to avoid accidental pointer manipulation while typing.�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  hhhh,hNubh�)��}�(h��Some keys do not trigger the timeout, specifically some modifier keys
(Ctrl, Alt, Shift, and Fn). Actions such as Ctrl + click thus stay
responsive.�h]�hE)��}�(h��Some keys do not trigger the timeout, specifically some modifier keys
(Ctrl, Alt, Shift, and Fn). Actions such as Ctrl + click thus stay
responsive.�h]�h9��Some keys do not trigger the timeout, specifically some modifier keys
(Ctrl, Alt, Shift, and Fn). Actions such as Ctrl + click thus stay
responsive.�����}�(hj   hj�  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  hhhh,hNubh�)��}�(h��Touches started while typing do not control the cursor even after typing
has stopped, it is thus possible to rest the palm on the touchpad while
typing.�h]�hE)��}�(h��Touches started while typing do not control the cursor even after typing
has stopped, it is thus possible to rest the palm on the touchpad while
typing.�h]�h9��Touches started while typing do not control the cursor even after typing
has stopped, it is thus possible to rest the palm on the touchpad while
typing.�����}�(hj  hj  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  hhhh,hNubh�)��}�(h�zPhysical buttons work even while the touchpad is disabled. This includes
:ref:`software-emulated buttons <t440_support>`.
�h]�hE)��}�(h�yPhysical buttons work even while the touchpad is disabled. This includes
:ref:`software-emulated buttons <t440_support>`.�h]�(h9�IPhysical buttons work even while the touchpad is disabled. This includes
�����}�(h�IPhysical buttons work even while the touchpad is disabled. This includes
�hj.  ubh�)��}�(h�/:ref:`software-emulated buttons <t440_support>`�h]�h�)��}�(hj9  h]�h9�software-emulated buttons�����}�(hhhj;  ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hj7  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�jE  �refexplicit��h��t440_support�h�h�h��uhh�hh,hK�hj.  ubh9�.�����}�(hj�  hj.  ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj*  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  hhhh,hNubeh}�(h]�h]�h]�h]�h]�j�  j�  uhh~hh,hK�hj�  hhubhE)��}�(h�iDisable-while-typing can be enabled and disabled by calling
**libinput_device_config_dwt_set_enabled()**.�h]�(h9�<Disable-while-typing can be enabled and disabled by calling
�����}�(h�<Disable-while-typing can be enabled and disabled by calling
�hjk  hhhNhNubh �strong���)��}�(h�,**libinput_device_config_dwt_set_enabled()**�h]�h9�(libinput_device_config_dwt_set_enabled()�����}�(hhhjv  ubah}�(h]�h]�h]�h]�h]�uhjt  hjk  ubh9�.�����}�(hj�  hjk  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubh)��}�(h�.. _stylus-touch-arbitration:�h]�h}�(h]�h]�h]�h]�h]�h*�stylus-touch-arbitration�uhhhK�hj�  hhhh,ubeh}�(h]�(j�  �id3�eh]�h]��disable-while-typing�ah]��disable-while-typing�ah]�uhh-hh/hhhh,hK�j:  Kj]  }�j�  jx  sj_  }�j�  jx  subh.)��}�(hhh]�(h3)��}�(h�Stylus-touch arbitration�h]�h9�Stylus-touch arbitration�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hK�ubhE)��}�(hX:  A special case of palm detection is touch arbitration on devices that
support styli. When interacting with a stylus on the screen, parts of the
hand may touch the surface and trigger touches. As the user is currently
interacting with the stylus, these touches would interfer with the correct
working of the stylus.�h]�h9X:  A special case of palm detection is touch arbitration on devices that
support styli. When interacting with a stylus on the screen, parts of the
hand may touch the surface and trigger touches. As the user is currently
interacting with the stylus, these touches would interfer with the correct
working of the stylus.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubhE)��}�(h�libinput employs a method similar to :ref:`disable-while-typing` to detect
these touches and disables the touchpad accordingly.�h]�(h9�%libinput employs a method similar to �����}�(h�%libinput employs a method similar to �hj�  hhhNhNubh�)��}�(h�:ref:`disable-while-typing`�h]�h�)��}�(hj�  h]�h9�disable-while-typing�����}�(hhhj�  ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��h��disable-while-typing�h�h�h��uhh�hh,hK�hj�  ubh9�? to detect
these touches and disables the touchpad accordingly.�����}�(h�? to detect
these touches and disables the touchpad accordingly.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubh)��}�(h�.. _thumb-detection:�h]�h}�(h]�h]�h]�h]�h]�h*�thumb-detection�uhhhK�hj�  hhhh,ubeh}�(h]�(j�  �id4�eh]�h]�(�stylus-touch arbitration��stylus-touch-arbitration�eh]�h]�uhh-hh/hhhh,hK�j]  }�j  j�  sj_  }�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�Thumb detection�h]�h9�Thumb detection�����}�(hj  hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj  hhhh,hK�ubhE)��}�(hX]  Many users rest their thumb on the touchpad while using the index finger to
move the finger around. For clicks, often the thumb is used rather than the
finger. The thumb should otherwise be ignored as a touch, i.e. it should not
count towards :ref:`clickfinger` and it should not cause a single-finger
movement to trigger :ref:`twofinger_scrolling`.�h]�(h9��Many users rest their thumb on the touchpad while using the index finger to
move the finger around. For clicks, often the thumb is used rather than the
finger. The thumb should otherwise be ignored as a touch, i.e. it should not
count towards �����}�(h��Many users rest their thumb on the touchpad while using the index finger to
move the finger around. For clicks, often the thumb is used rather than the
finger. The thumb should otherwise be ignored as a touch, i.e. it should not
count towards �hj  hhhNhNubh�)��}�(h�:ref:`clickfinger`�h]�h�)��}�(hj'  h]�h9�clickfinger�����}�(hhhj)  ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hj%  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j3  �refexplicit��h��clickfinger�h�h�h��uhh�hh,hK�hj  ubh9�= and it should not cause a single-finger
movement to trigger �����}�(h�= and it should not cause a single-finger
movement to trigger �hj  hhhNhNubh�)��}�(h�:ref:`twofinger_scrolling`�h]�h�)��}�(hjJ  h]�h9�twofinger_scrolling�����}�(hhhjL  ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hjH  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�jV  �refexplicit��h��twofinger_scrolling�h�h�h��uhh�hh,hK�hj  ubh9�.�����}�(hj�  hj  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj  hhubhE)��}�(h��libinput uses two triggers for thumb detection: pressure and
location. A touch exceeding a pressure threshold is considered a thumb if it
is within the thumb detection zone.�h]�h9��libinput uses two triggers for thumb detection: pressure and
location. A touch exceeding a pressure threshold is considered a thumb if it
is within the thumb detection zone.�����}�(hjr  hjp  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj  hhubh �note���)��}�(h��"Pressure" on touchpads is synonymous with "contact area." A large touch
surface area has a higher pressure and thus hints at a thumb or palm
touching the surface.�h]�hE)��}�(h��"Pressure" on touchpads is synonymous with "contact area." A large touch
surface area has a higher pressure and thus hints at a thumb or palm
touching the surface.�h]�h9��“Pressure” on touchpads is synonymous with “contact area.” A large touch
surface area has a higher pressure and thus hints at a thumb or palm
touching the surface.�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  ubah}�(h]�h]�h]�h]�h]�uhj~  hj  hhhh,hNubhE)��}�(h��Pressure readings are unreliable at the far bottom of the touchpad as a
thumb hanging mostly off the touchpad will have a small surface area.
libinput has a definitive thumb zone where any touch is considered a resting
thumb.�h]�h9��Pressure readings are unreliable at the far bottom of the touchpad as a
thumb hanging mostly off the touchpad will have a small surface area.
libinput has a definitive thumb zone where any touch is considered a resting
thumb.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj  hhubj�  )��}�(hhh]�j�  )��}�(h�3.. figure:: thumb-detection.svg
    :align: center
�h]�h}�(h]�h]�h]�h]�h]��uri��thumb-detection.svg�j�  }�j�  j�  suhj�  hj�  hh,hNubah}�(h]�h]�h]�h]�h]�j�  �center�uhj�  hj  hhhh,hNubhE)��}�(hX  The picture above shows the two detection areas. In the larger (light red)
area, a touch is labelled as thumb when it exceeds a device-specific
pressure threshold. In the lower (dark red) area, a touch is labelled as
thumb if it remains in that area for a time without moving outside.�h]�h9X  The picture above shows the two detection areas. In the larger (light red)
area, a touch is labelled as thumb when it exceeds a device-specific
pressure threshold. In the lower (dark red) area, a touch is labelled as
thumb if it remains in that area for a time without moving outside.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj  hhubeh}�(h]�(j�  �id5�eh]�h]�(�thumb detection��thumb-detection�eh]�h]�uhh-hh/hhhh,hK�j]  }�j�  j�  sj_  }�j�  j�  subeh}�(h]�(h+�id1�eh]�h]�(�palm detection��palm_detection�eh]�h]�uhh-hhhhhh,hKj]  }�j�  h sj_  }�h+h subeh}�(h]�h]�h]�h]�h]��source�h,uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h2N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`d2f4222`�h]�j  )��}�(h�git commit d2f4222�h]�h9�git commit d2f4222�����}�(hhhjB  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/d2f4222�uhj  hj>  ubah}�(h]�h]�h]�j;  ah]�h]�uhj<  h�<rst_prolog>�hKhhub�git_version_full�j=  )��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f7878870ea0>`

�h]�j  )��}�(h�<git commit <function get_git_version_full at 0x7f7878870ea0>�h]�h9�<git commit <function get_git_version_full at 0x7f7878870ea0>�����}�(hhhj_  ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f7878870ea0>�uhj  hj[  ubah}�(h]�h]�h]�jZ  ah]�h]�uhj<  hjY  hKhhubu�substitution_names�}�(�git_version�j;  �git_version_full�jZ  u�refnames�}��refids�}�(h+]�h aj   ]�j�  ajS  ]�jI  aj�  ]�j�  aj6  ]�j,  aj�  ]�j�  aj�  ]�jx  aj�  ]�j�  aj�  ]�j�  au�nameids�}�(j�  h+j�  j�  jZ  j   jY  jV  j�  jS  j�  j�  j=  j�  j<  j9  j�  j6  j�  j�  j�  j�  j�  j�  j6  j3  j�  j�  j�  j�  j  j�  j  j  j�  j�  j�  j�  u�	nametypes�}�(j�  �j�  NjZ  �jY  Nj�  �j�  Nj=  �j<  Nj�  �j�  Nj�  �j�  Nj6  �j�  �j�  �j  �j  Nj�  �j�  Nuh}�(h+h/j�  h/j   j  jV  j  jS  ja  j�  ja  j�  j�  j9  j�  j6  jB  j�  jB  j�  j�  j�  j�  j3  j-  j�  j�  j�  j�  j�  j�  j�  j�  j  j�  j�  j  j�  j  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�h �system_message���)��}�(hhh]�hE)��}�(h�7Duplicate implicit target name: "disable-while-typing".�h]�h9�;Duplicate implicit target name: “disable-while-typing”.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]�j�  a�level�K�type��INFO��source�h,�line�K�uhj�  hj�  hhhh,hK�uba�transform_messages�]�(j�  )��}�(hhh]�hE)��}�(hhh]�h9�4Hyperlink target "palm-detection" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�Kuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�/Hyperlink target "palm-tool" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K(uhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�3Hyperlink target "palm-pressure" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K3uhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�5Hyperlink target "palm-touch-size" is not referenced.�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhhDhj  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�KCuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�:Hyperlink target "palm-exclusion-zones" is not referenced.�����}�(hhhj,  ubah}�(h]�h]�h]�h]�h]�uhhDhj)  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�KQuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�:Hyperlink target "trackpoint-disabling" is not referenced.�����}�(hhhjF  ubah}�(h]�h]�h]�h]�h]�uhhDhjC  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�Kwuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�:Hyperlink target "disable-while-typing" is not referenced.�����}�(hhhj`  ubah}�(h]�h]�h]�h]�h]�uhhDhj]  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K�uhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�>Hyperlink target "stylus-touch-arbitration" is not referenced.�����}�(hhhjz  ubah}�(h]�h]�h]�h]�h]�uhhDhjw  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K�uhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�5Hyperlink target "thumb-detection" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K�uhj�  ube�transformer�N�
decoration�Nhhub.