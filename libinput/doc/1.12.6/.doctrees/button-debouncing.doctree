��J-      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`9da9118`�h]�h �	reference���)��}�(h�git commit 9da9118�h]�h �Text����git commit 9da9118�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/9da9118�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�^.. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f81e92b1598>`


�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7f81e92b1598>�h]�h�<git commit <function get_git_version_full at 0x7f81e92b1598>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f81e92b1598>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _button_debouncing:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��button-debouncing�uh0h]h:Kh hhhh8�=/home/whot/code/libinput/build/doc/user/button-debouncing.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Button debouncing�h]�h�Button debouncing�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh �	paragraph���)��}�(hX  Physical buttons experience wear-and-tear with usage. On some devices this
can result in an effect called "contact bouncing" or "chatter". This effect
can cause the button to send multiple events within a short time frame, even
though the user only pressed or clicked the button once. This effect can be
counteracted by "debouncing" the buttons, usually by ignoring erroneous
events.�h]�hX�  Physical buttons experience wear-and-tear with usage. On some devices this
can result in an effect called “contact bouncing” or “chatter”. This effect
can cause the button to send multiple events within a short time frame, even
though the user only pressed or clicked the button once. This effect can be
counteracted by “debouncing” the buttons, usually by ignoring erroneous
events.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(h�hlibinput provides two methods of debouncing buttons, referred to as the
"bounce" and "spurious" methods:�h]�h�plibinput provides two methods of debouncing buttons, referred to as the
“bounce” and “spurious” methods:�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(hXM  In the "bounce" method, libinput monitors hardware bouncing on button
state changes, i.e. when a user clicks or releases a button. For example,
if a user presses a button but the hardware generates a
press-release-press sequence in quick succession, libinput ignores the
release and second press event. This method is always enabled.�h]�h�)��}�(hXM  In the "bounce" method, libinput monitors hardware bouncing on button
state changes, i.e. when a user clicks or releases a button. For example,
if a user presses a button but the hardware generates a
press-release-press sequence in quick succession, libinput ignores the
release and second press event. This method is always enabled.�h]�hXQ  In the “bounce” method, libinput monitors hardware bouncing on button
state changes, i.e. when a user clicks or releases a button. For example,
if a user presses a button but the hardware generates a
press-release-press sequence in quick succession, libinput ignores the
release and second press event. This method is always enabled.�����}�(hh�h h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�hhh8hkh:Nubh�)��}�(hXk  in the "spurious" method, libinput detects spurious releases of a button
while the button is physically held down by the user. These releases are
immediately followed by a press event. libinput monitors for these events
and ignores the release and press event. This method is disabled by
default and enables once libinput detects the first faulty event sequence.
�h]�h�)��}�(hXj  in the "spurious" method, libinput detects spurious releases of a button
while the button is physically held down by the user. These releases are
immediately followed by a press event. libinput monitors for these events
and ignores the release and press event. This method is disabled by
default and enables once libinput detects the first faulty event sequence.�h]�hXn  in the “spurious” method, libinput detects spurious releases of a button
while the button is physically held down by the user. These releases are
immediately followed by a press event. libinput monitors for these events
and ignores the release and press event. This method is disabled by
default and enables once libinput detects the first faulty event sequence.�����}�(hh�h h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�hhh8hkh:Nubeh!}�(h#]�h%]�h']�h)]�h+]��bullet��-�uh0h�h8hkh:Kh hnhhubh�)��}�(hX]  The "bounce" method guarantees that all press events are delivered
immediately and most release events are delivered immediately. The
"spurious" method requires that release events are delayed, libinput thus
does not enable this method unless a faulty event sequence is detected. A
message is printed to the log when spurious deboucing was detected.�h]�hXe  The “bounce” method guarantees that all press events are delivered
immediately and most release events are delivered immediately. The
“spurious” method requires that release events are delayed, libinput thus
does not enable this method unless a faulty event sequence is detected. A
message is printed to the log when spurious deboucing was detected.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(hX�  libinput's debouncing is supposed to correct hardware damage or
substandard hardware. Debouncing also exists as an accessibility feature
but the requirements are different. In the accessibility feature, multiple
physical key presses, usually caused by involuntary muscle movement, must be
filtered to only one key press. This feature must be implemented higher in
the stack, libinput is limited to hardware debouncing.�h]�hX�  libinput’s debouncing is supposed to correct hardware damage or
substandard hardware. Debouncing also exists as an accessibility feature
but the requirements are different. In the accessibility feature, multiple
physical key presses, usually caused by involuntary muscle movement, must be
filtered to only one key press. This feature must be implemented higher in
the stack, libinput is limited to hardware debouncing.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K#h hnhhubh�)��}�(hXf  Below is an illustration of the button debouncing modes to show the relation
of the physical button state and the application state. Where applicable, an
extra line is added to show the timeouts used by libinput that
affect the button state handling. The waveform's high and low states
correspond to the buttons 'pressed' and 'released' states, respectively.�h]�hXp  Below is an illustration of the button debouncing modes to show the relation
of the physical button state and the application state. Where applicable, an
extra line is added to show the timeouts used by libinput that
affect the button state handling. The waveform’s high and low states
correspond to the buttons ‘pressed’ and ‘released’ states, respectively.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K*h hnhhubh �figure���)��}�(hhh]�(h �image���)��}�(h�o.. figure:: button-debouncing-wave-diagram.svg
    :align: center

    Diagram illustrating button debouncing

�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��"button-debouncing-wave-diagram.svg��
candidates�}��*�j  suh0j  h j
  h8hkh:K3ubh �caption���)��}�(h�&Diagram illustrating button debouncing�h]�h�&Diagram illustrating button debouncing�����}�(hj"  h j   ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j  h8hkh:K3h j
  ubeh!}�(h#]��id2�ah%]�h']�h)]�h+]��align��center�uh0j  h:K3h hnhhh8hkubh�)��}�(h��Some devices send events in bursts, erroneously triggering the button
debouncing detection. Please :ref:`file a bug <reporting_bugs>` if that
occurs for your device.�h]�(h�cSome devices send events in bursts, erroneously triggering the button
debouncing detection. Please �����}�(h�cSome devices send events in bursts, erroneously triggering the button
debouncing detection. Please �h j7  hhh8Nh:Nub�sphinx.addnodes��pending_xref���)��}�(h�":ref:`file a bug <reporting_bugs>`�h]�h �inline���)��}�(hjE  h]�h�
file a bug�����}�(hhh jI  ubah!}�(h#]�h%]�(�xref��std��std-ref�eh']�h)]�h+]�uh0jG  h jC  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�jT  �refexplicit���	reftarget��reporting_bugs��refdoc��button-debouncing��refwarn��uh0jA  h8hkh:K6h j7  ubh�  if that
occurs for your device.�����}�(h�  if that
occurs for your device.�h j7  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K6h hnhhubeh!}�(h#]�(hj�id1�eh%]�h']�(�button debouncing��button_debouncing�eh)]�h+]�uh0hlh hhhh8hkh:K�expect_referenced_by_name�}�jy  h_s�expect_referenced_by_id�}�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�hj]�h_as�nameids�}�(jy  hjjx  ju  u�	nametypes�}�(jy  �jx  Nuh#}�(hjhnju  hnj0  j
  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�h �system_message���)��}�(hhh]�h�)��}�(hhh]�h�7Hyperlink target "button-debouncing" is not referenced.�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type��INFO��source�hk�line�Kuh0j   uba�transformer�N�
decoration�Nhhub.