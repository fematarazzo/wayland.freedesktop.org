���]      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`9da9118`�h]�h �	reference���)��}�(h�git commit 9da9118�h]�h �Text����git commit 9da9118�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/9da9118�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f81e92b1598>`

�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7f81e92b1598>�h]�h�<git commit <function get_git_version_full at 0x7f81e92b1598>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f81e92b1598>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _absolute_axes:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��absolute-axes�uh0h]h:Kh hhhh8�9/home/whot/code/libinput/build/doc/user/absolute-axes.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Absolute axes�h]�h�Absolute axes�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh �	paragraph���)��}�(hX  Devices with absolute axes are those that send positioning data for an axis in
a device-specific coordinate range, defined by a minimum and a maximum value.
Compare this to relative devices (e.g. a mouse) that can only detect
directional data, not positional data.�h]�hX  Devices with absolute axes are those that send positioning data for an axis in
a device-specific coordinate range, defined by a minimum and a maximum value.
Compare this to relative devices (e.g. a mouse) that can only detect
directional data, not positional data.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(h�<libinput supports three types of devices with absolute axes:�h]�h�<libinput supports three types of devices with absolute axes:�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh
)��}�(hhh]�h �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�multi-touch screens�h]�h�)��}�(hh�h]�h�multi-touch screens�����}�(hh�h h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh�)��}�(h�single-touch screens�h]�h�)��}�(hh�h]�h�single-touch screens�����}�(hh�h h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh�)��}�(h�):ref:`graphics tablets <tablet-support>`
�h]�h�)��}�(h�(:ref:`graphics tablets <tablet-support>`�h]��sphinx.addnodes��pending_xref���)��}�(hh�h]�h �inline���)��}�(hh�h]�h�graphics tablets�����}�(hhh h�ubah!}�(h#]�h%]�(�xref��std��std-ref�eh']�h)]�h+]�uh0h�h h�ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�h�refexplicit���	reftarget��tablet-support��refdoc��absolute-axes��refwarn��uh0h�h8hkh:Kh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubeh!}�(h#]�h%]�h']�h)]�h+]��bullet��-�uh0h�h8hkh:Kh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hnhhh8Nh:Nubh�)��}�(h��Touchpads are technically absolute devices but libinput converts the axis values
to directional motion and posts events as relative events. Touchpads do not count
as absolute devices in libinput.�h]�h��Touchpads are technically absolute devices but libinput converts the axis values
to directional motion and posts events as relative events. Touchpads do not count
as absolute devices in libinput.�����}�(hj"  h j   hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(hX�  For all absolute devices in libinput, the default unit for x/y coordinates is
in mm off the top left corner on the device, or more specifically off the
device's sensor. If the device is physically rotated from its natural
position and this rotation was communicated to libinput (e.g. by setting
the device left-handed),
the coordinate origin is the top left corner in the current rotation.�h]�hX�  For all absolute devices in libinput, the default unit for x/y coordinates is
in mm off the top left corner on the device, or more specifically off the
device’s sensor. If the device is physically rotated from its natural
position and this rotation was communicated to libinput (e.g. by setting
the device left-handed),
the coordinate origin is the top left corner in the current rotation.�����}�(hj0  h j.  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh^)��}�(h�.. _absolute_axes_handling:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�absolute-axes-handling�uh0h]h:K"h hnhhh8hkubhm)��}�(hhh]�(hr)��}�(h� Handling of absolute coordinates�h]�h� Handling of absolute coordinates�����}�(hjL  h jJ  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh jG  hhh8hkh:K!ubh�)��}�(hX>  In most use-cases, absolute input devices are mapped to a single screen. For
direct input devices such as touchscreens the aspect ratio of the screen and
the device match. Mapping the input device position to the output position is
thus a simple mapping between two coordinates. libinput provides the API for
this with�h]�hX>  In most use-cases, absolute input devices are mapped to a single screen. For
direct input devices such as touchscreens the aspect ratio of the screen and
the device match. Mapping the input device position to the output position is
thus a simple mapping between two coordinates. libinput provides the API for
this with�����}�(hjZ  h jX  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K#h jG  hhubh�)��}�(hhh]�(h�)��}�(h�J**libinput_event_pointer_get_absolute_x_transformed()** for pointer events�h]�h�)��}�(hjk  h]�(h �strong���)��}�(h�7**libinput_event_pointer_get_absolute_x_transformed()**�h]�h�3libinput_event_pointer_get_absolute_x_transformed()�����}�(hhh jr  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jp  h jm  ubh� for pointer events�����}�(h� for pointer events�h jm  ubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K)h ji  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jf  hhh8hkh:Nubh�)��}�(h�>**libinput_event_touch_get_x_transformed()** for touch events
�h]�h�)��}�(h�=**libinput_event_touch_get_x_transformed()** for touch events�h]�(jq  )��}�(h�,**libinput_event_touch_get_x_transformed()**�h]�h�(libinput_event_touch_get_x_transformed()�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jp  h j�  ubh� for touch events�����}�(h� for touch events�h j�  ubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K*h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jf  hhh8hkh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�j  j  uh0h�h8hkh:K)h jG  hhubh�)��}�(hX4  libinput's API only provides the call to map into a single coordinate range.
If the coordinate range has an offset, the compositor is responsible for
applying that offset after the mapping. For example, if the device is mapped
to the right of two outputs, add the output offset to the transformed
coordinate.�h]�hX6  libinput’s API only provides the call to map into a single coordinate range.
If the coordinate range has an offset, the compositor is responsible for
applying that offset after the mapping. For example, if the device is mapped
to the right of two outputs, add the output offset to the transformed
coordinate.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K,h jG  hhubh^)��}�(h�.. _absolute_axes_nores:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�absolute-axes-nores�uh0h]h:K7h jG  hhh8hkubeh!}�(h#]�(� handling-of-absolute-coordinates�jF  eh%]�h']�(� handling of absolute coordinates��absolute_axes_handling�eh)]�h+]�uh0hlh hnhhh8hkh:K!�expect_referenced_by_name�}�j�  j<  s�expect_referenced_by_id�}�jF  j<  subhm)��}�(hhh]�(hr)��}�(h�Devices without x/y resolution�h]�h�Devices without x/y resolution�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:K6ubh�)��}�(hX  An absolute device that does not provide a valid resolution is considered
buggy and must be fixed in the kernel. Some touchpad devices do not
provide resolution, those devices are correctly handled within libinput
(touchpads are not absolute devices, as mentioned above).�h]�hX  An absolute device that does not provide a valid resolution is considered
buggy and must be fixed in the kernel. Some touchpad devices do not
provide resolution, those devices are correctly handled within libinput
(touchpads are not absolute devices, as mentioned above).�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K8h j�  hhubh^)��}�(h�.. _calibration:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�calibration�uh0h]h:KBh j�  hhh8hkubeh!}�(h#]�(�devices-without-x-y-resolution�j�  eh%]�h']�(�devices without x/y resolution��absolute_axes_nores�eh)]�h+]�uh0hlh hnhhh8hkh:K6j�  }�j  j�  sj�  }�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�Calibration of absolute devices�h]�h�Calibration of absolute devices�����}�(hj  h j  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j  hhh8hkh:KAubh�)��}�(h��Absolute devices may require calibration to map precisely into the output
range required. This is done by setting a transformation matrix, see
**libinput_device_config_calibration_set_matrix()** which is applied to
each input coordinate.�h]�(h��Absolute devices may require calibration to map precisely into the output
range required. This is done by setting a transformation matrix, see
�����}�(h��Absolute devices may require calibration to map precisely into the output
range required. This is done by setting a transformation matrix, see
�h j*  hhh8Nh:Nubjq  )��}�(h�3**libinput_device_config_calibration_set_matrix()**�h]�h�/libinput_device_config_calibration_set_matrix()�����}�(hhh j3  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jp  h j*  ubh�+ which is applied to
each input coordinate.�����}�(h�+ which is applied to
each input coordinate.�h j*  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KCh j  hhub�sphinx.ext.mathbase��displaymath���)��}�(hhh]�h!}�(h#]�h%]�h']�h)]�h+]��latex���\begin{pmatrix}
 cos\theta & -sin\theta & xoff \\
 sin\theta &  cos\theta & yoff \\
 0   & 0    & 1
\end{pmatrix} \begin{pmatrix}
x \\ y \\ 1
\end{pmatrix}

��number�N�label�N�nowrap���docname�j  uh0jM  h8hkh:KHh j  hhubh�)��}�(hX5  :math:`\theta` is the rotation angle. The offsets :math:`xoff` and :math:`yoff` are
specified in device dimensions, i.e. a value of 1 equals one device width or
height. Note that rotation applies to the device's origin, rotation usually
requires an offset to move the coordinates back into the original range.�h]�(jL  �math���)��}�(hhh]�h!}�(h#]�h%]�h']�h)]�h+]��latex��\theta�uh0jb  h j^  ubh�$ is the rotation angle. The offsets �����}�(h�$ is the rotation angle. The offsets �h j^  hhh8Nh:Nubjc  )��}�(hhh]�h!}�(h#]�h%]�h']�h)]�h+]��latex��xoff�uh0jb  h j^  ubh� and �����}�(h� and �h j^  hhh8Nh:Nubjc  )��}�(hhh]�h!}�(h#]�h%]�h']�h)]�h+]��latex��yoff�uh0jb  h j^  ubh�� are
specified in device dimensions, i.e. a value of 1 equals one device width or
height. Note that rotation applies to the device’s origin, rotation usually
requires an offset to move the coordinates back into the original range.�����}�(h�� are
specified in device dimensions, i.e. a value of 1 equals one device width or
height. Note that rotation applies to the device's origin, rotation usually
requires an offset to move the coordinates back into the original range.�h j^  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KQh j  hhubh�)��}�(h�The most common matrices are:�h]�h�The most common matrices are:�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KVh j  hhubh�)��}�(hhh]�(h�)��}�(h��90 degree clockwise:
   .. math::
       \begin{pmatrix}
        0 & -1 & 1 \\
        1 & 0 & 0 \\
        0 & 0 & 1
       \end{pmatrix}�h]�h �definition_list���)��}�(hhh]�h �definition_list_item���)��}�(h�x90 degree clockwise:
.. math::
    \begin{pmatrix}
     0 & -1 & 1 \\
     1 & 0 & 0 \\
     0 & 0 & 1
    \end{pmatrix}�h]�(h �term���)��}�(h�90 degree clockwise:�h]�h�90 degree clockwise:�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h8hkh:K]h j�  ubh �
definition���)��}�(hhh]�jN  )��}�(hhh]�h!}�(h#]�h%]�h']�h)]�h+]�jX  �G\begin{pmatrix}
 0 & -1 & 1 \\
 1 & 0 & 0 \\
 0 & 0 & 1
\end{pmatrix}

�jZ  Nj[  Nj\  �j]  j  uh0jM  h8hkh:KYh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j�  ubeh!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h8hkh:K]h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  hhh8Nh:Nubh�)��}�(h��180 degree clockwise:
   .. math::
       \begin{pmatrix}
        -1 & 0 & 1 \\
        0 & -1 & 1 \\
        0 & 0 & 1
       \end{pmatrix}�h]�j�  )��}�(hhh]�j�  )��}�(h�z180 degree clockwise:
.. math::
    \begin{pmatrix}
     -1 & 0 & 1 \\
     0 & -1 & 1 \\
     0 & 0 & 1
    \end{pmatrix}�h]�(j�  )��}�(h�180 degree clockwise:�h]�h�180 degree clockwise:�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h8hkh:Kdh j�  ubj�  )��}�(hhh]�jN  )��}�(hhh]�h!}�(h#]�h%]�h']�h)]�h+]�jX  �H\begin{pmatrix}
 -1 & 0 & 1 \\
 0 & -1 & 1 \\
 0 & 0 & 1
\end{pmatrix}

�jZ  Nj[  Nj\  �j]  j  uh0jM  h8hkh:K`h j
  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j�  ubeh!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h8hkh:Kdh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  hhh8Nh:Nubh�)��}�(h��270 degree clockwise:
   .. math::
       \begin{pmatrix}
        0 & 1 & 0 \\
        -1 & 0 & 1 \\
        0 & 0 & 1
       \end{pmatrix}�h]�j�  )��}�(hhh]�j�  )��}�(h�y270 degree clockwise:
.. math::
    \begin{pmatrix}
     0 & 1 & 0 \\
     -1 & 0 & 1 \\
     0 & 0 & 1
    \end{pmatrix}�h]�(j�  )��}�(h�270 degree clockwise:�h]�h�270 degree clockwise:�����}�(hj<  h j:  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h8hkh:Kkh j6  ubj�  )��}�(hhh]�jN  )��}�(hhh]�h!}�(h#]�h%]�h']�h)]�h+]�jX  �G\begin{pmatrix}
 0 & 1 & 0 \\
 -1 & 0 & 1 \\
 0 & 0 & 1
\end{pmatrix}

�jZ  Nj[  Nj\  �j]  j  uh0jM  h8hkh:Kgh jH  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j6  ubeh!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h8hkh:Kkh j3  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j/  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  hhh8Nh:Nubh�)��}�(h��reflection along y axis:
   .. math::
       \begin{pmatrix}
        -1 & 0 & 1 \\
        1 & 0 & 0 \\
        0 & 0 & 1
       \end{pmatrix}
�h]�j�  )��}�(hhh]�j�  )��}�(h�}reflection along y axis:
.. math::
    \begin{pmatrix}
     -1 & 0 & 1 \\
     1 & 0 & 0 \\
     0 & 0 & 1
    \end{pmatrix}
�h]�(j�  )��}�(h�reflection along y axis:�h]�h�reflection along y axis:�����}�(hjz  h jx  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h8hkh:Ksh jt  ubj�  )��}�(hhh]�jN  )��}�(hhh]�h!}�(h#]�h%]�h']�h)]�h+]�jX  �G\begin{pmatrix}
 -1 & 0 & 1 \\
 1 & 0 & 0 \\
 0 & 0 & 1
\end{pmatrix}

�jZ  Nj[  Nj\  �j]  j  uh0jM  h8hkh:Knh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h jt  ubeh!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h8hkh:Ksh jq  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h jm  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�j  j  uh0h�h8hkh:KXh j  hhubh�)��}�(hX  See Wikipedia's
`Transformation Matrix article <http://en.wikipedia.org/wiki/Transformation_matrix>`_
for more information on the matrix maths. See
**libinput_device_config_calibration_get_default_matrix()** for how these
matrices must be supplied to libinput.�h]�(h�See Wikipedia’s
�����}�(h�See Wikipedia's
�h j�  hhh8Nh:Nubh)��}�(h�U`Transformation Matrix article <http://en.wikipedia.org/wiki/Transformation_matrix>`_�h]�h�Transformation Matrix article�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]��name��Transformation Matrix article��refuri��2http://en.wikipedia.org/wiki/Transformation_matrix�uh0hh j�  ubh^)��}�(h�5 <http://en.wikipedia.org/wiki/Transformation_matrix>�h]�h!}�(h#]��transformation-matrix-article�ah%]�h']��transformation matrix article�ah)]�h+]��refuri�j�  uh0h]�
referenced�Kh j�  ubh�/
for more information on the matrix maths. See
�����}�(h�/
for more information on the matrix maths. See
�h j�  hhh8Nh:Nubjq  )��}�(h�;**libinput_device_config_calibration_get_default_matrix()**�h]�h�7libinput_device_config_calibration_get_default_matrix()�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jp  h j�  ubh�5 for how these
matrices must be supplied to libinput.�����}�(h�5 for how these
matrices must be supplied to libinput.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kuh j  hhubh�)��}�(h��Once applied, any x and y axis value has the calibration applied before it
is made available to the caller. libinput does not provide access to the
raw coordinates before the calibration is applied.�h]�h��Once applied, any x and y axis value has the calibration applied before it
is made available to the caller. libinput does not provide access to the
raw coordinates before the calibration is applied.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K{h j  hhubh^)��}�(h�.. _absolute_axes_nonorm:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�absolute-axes-nonorm�uh0h]h:K�h j  hhh8hkubeh!}�(h#]�(�calibration-of-absolute-devices�j  eh%]�h']�(�calibration of absolute devices��calibration�eh)]�h+]�uh0hlh hnhhh8hkh:KAj�  }�j  j  sj�  }�j  j  subhm)��}�(hhh]�(hr)��}�(h�&Why x/y coordinates are not normalized�h]�h�&Why x/y coordinates are not normalized�����}�(hj!  h j  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j  hhh8hkh:K�ubh�)��}�(hX�  x/y are not given in :ref:`normalized coordinates <motion_normalization>`
([0..1]) for one simple reason: the aspect ratio of virtually all current
devices is something other than 1:1. A normalized axes thus is only useful to
determine that the stylus is e.g. at 78% from the left, 34% from the top of
the device. Without knowing the per-axis resolution, these numbers are
meaningless. Worse, calculation based on previous coordinates is simply wrong:
a movement from 0/0 to 50%/50% is not a 45-degree line.�h]�(h�x/y are not given in �����}�(h�x/y are not given in �h j-  hhh8Nh:Nubh�)��}�(h�4:ref:`normalized coordinates <motion_normalization>`�h]�h�)��}�(hj8  h]�h�normalized coordinates�����}�(hhh j:  ubah!}�(h#]�h%]�(h�std��std-ref�eh']�h)]�h+]�uh0h�h j6  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�jD  �refexplicit��j  �motion_normalization�j  j  j  �uh0h�h8hkh:K�h j-  ubhX�  
([0..1]) for one simple reason: the aspect ratio of virtually all current
devices is something other than 1:1. A normalized axes thus is only useful to
determine that the stylus is e.g. at 78% from the left, 34% from the top of
the device. Without knowing the per-axis resolution, these numbers are
meaningless. Worse, calculation based on previous coordinates is simply wrong:
a movement from 0/0 to 50%/50% is not a 45-degree line.�����}�(hX�  
([0..1]) for one simple reason: the aspect ratio of virtually all current
devices is something other than 1:1. A normalized axes thus is only useful to
determine that the stylus is e.g. at 78% from the left, 34% from the top of
the device. Without knowing the per-axis resolution, these numbers are
meaningless. Worse, calculation based on previous coordinates is simply wrong:
a movement from 0/0 to 50%/50% is not a 45-degree line.�h j-  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j  hhubh�)��}�(h��This could be alleviated by providing resolution and information about the
aspect ratio to the caller. Which shifts processing and likely errors into the
caller for little benefit. Providing the x/y axes in mm from the outset
removes these errors.�h]�h��This could be alleviated by providing resolution and information about the
aspect ratio to the caller. Which shifts processing and likely errors into the
caller for little benefit. Providing the x/y axes in mm from the outset
removes these errors.�����}�(hja  h j_  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j  hhubeh!}�(h#]�(�&why-x-y-coordinates-are-not-normalized�j  eh%]�h']�(�&why x/y coordinates are not normalized��absolute_axes_nonorm�eh)]�h+]�uh0hlh hnhhh8hkh:K�j�  }�js  j  sj�  }�j  j  subeh!}�(h#]�(hj�id1�eh%]�h']�(�absolute axes��absolute_axes�eh)]�h+]�uh0hlh hhhh8hkh:Kj�  }�j~  h_sj�  }�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�(hj]�h_ajF  ]�j<  aj�  ]�j�  aj  ]�j  aj  ]�j  au�nameids�}�(j~  hjj}  jz  j�  jF  j�  j�  j  j�  j  j  j  j  j  j  j�  j�  js  j  jr  jo  u�	nametypes�}�(j~  �j}  Nj�  �j�  Nj  �j  Nj  �j  Nj�  �js  �jr  Nuh#}�(hjhnjz  hnjF  jG  j�  jG  j�  j�  j  j�  j  j  j  j  j�  j�  j  j  jo  j  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�h�)��}�(hhh]�h�3Hyperlink target "absolute-axes" is not referenced.�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j	  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type��INFO��source�hk�line�Kuh0j  ubj  )��}�(hhh]�h�)��}�(hhh]�h�<Hyperlink target "absolute-axes-handling" is not referenced.�����}�(hhh j'  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j$  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j!  �source�hk�line�K"uh0j  ubj  )��}�(hhh]�h�)��}�(hhh]�h�9Hyperlink target "absolute-axes-nores" is not referenced.�����}�(hhh jA  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j>  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j!  �source�hk�line�K7uh0j  ubj  )��}�(hhh]�h�)��}�(hhh]�h�1Hyperlink target "calibration" is not referenced.�����}�(hhh j[  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jX  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j!  �source�hk�line�KBuh0j  ubj  )��}�(hhh]�h�)��}�(hhh]�h�:Hyperlink target "absolute-axes-nonorm" is not referenced.�����}�(hhh ju  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jr  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j!  �source�hk�line�K�uh0j  ube�transformer�N�
decoration�Nhhub.