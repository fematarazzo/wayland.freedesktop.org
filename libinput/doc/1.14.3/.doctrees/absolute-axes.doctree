���a      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �target���)��}�(h�.. _absolute_axes:�h]�h}�(h]�h]�h]�h]�h]��refid��absolute-axes�uhhhKhhhhh�9/home/whot/code/libinput/build/doc/user/absolute-axes.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Absolute axes�h]�h �Text����Absolute axes�����}�(hh6hh4hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh/hhhh,hKubh �	paragraph���)��}�(hX  Devices with absolute axes are those that send positioning data for an axis in
a device-specific coordinate range, defined by a minimum and a maximum value.
Compare this to relative devices (e.g. a mouse) that can only detect
directional data, not positional data.�h]�h9X  Devices with absolute axes are those that send positioning data for an axis in
a device-specific coordinate range, defined by a minimum and a maximum value.
Compare this to relative devices (e.g. a mouse) that can only detect
directional data, not positional data.�����}�(hhHhhFhhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubhE)��}�(h�<libinput supports three types of devices with absolute axes:�h]�h9�<libinput supports three types of devices with absolute axes:�����}�(hhVhhThhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh
)��}�(hhh]�h �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�multi-touch screens�h]�hE)��}�(hhnh]�h9�multi-touch screens�����}�(hhnhhpubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhhlubah}�(h]�h]�h]�h]�h]�uhhjhhgubhk)��}�(h�single-touch screens�h]�hE)��}�(hh�h]�h9�single-touch screens�����}�(hh�hh�ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh�ubah}�(h]�h]�h]�h]�h]�uhhjhhgubhk)��}�(h�):ref:`graphics tablets <tablet-support>`
�h]�hE)��}�(h�(:ref:`graphics tablets <tablet-support>`�h]��sphinx.addnodes��pending_xref���)��}�(hh�h]�h �inline���)��}�(h�!graphics tablets <tablet-support>�h]�h9�graphics tablets�����}�(hhhh�ubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhh�hh�ubah}�(h]�h]�h]�h]�h]��refdoc��absolute-axes��	refdomain�h��reftype��ref��refexplicit���refwarn���	reftarget��tablet-support�uhh�hh,hKhh�ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh�ubah}�(h]�h]�h]�h]�h]�uhhjhhgubeh}�(h]�h]�h]�h]�h]��bullet��-�uhhehh,hKhhbubah}�(h]�h]�h]�h]�h]�uhh	hh/hhhNhNubhE)��}�(h��Touchpads are technically absolute devices but libinput converts the axis values
to directional motion and posts events as relative events. Touchpads do not count
as absolute devices in libinput.�h]�h9��Touchpads are technically absolute devices but libinput converts the axis values
to directional motion and posts events as relative events. Touchpads do not count
as absolute devices in libinput.�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubhE)��}�(hX�  For all absolute devices in libinput, the default unit for x/y coordinates is
in mm off the top left corner on the device, or more specifically off the
device's sensor. If the device is physically rotated from its natural
position and this rotation was communicated to libinput (e.g. by setting
the device left-handed),
the coordinate origin is the top left corner in the current rotation.�h]�h9X�  For all absolute devices in libinput, the default unit for x/y coordinates is
in mm off the top left corner on the device, or more specifically off the
device’s sensor. If the device is physically rotated from its natural
position and this rotation was communicated to libinput (e.g. by setting
the device left-handed),
the coordinate origin is the top left corner in the current rotation.�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh)��}�(h�.. _absolute_axes_handling:�h]�h}�(h]�h]�h]�h]�h]�h*�absolute-axes-handling�uhhhK"hh/hhhh,ubh.)��}�(hhh]�(h3)��}�(h� Handling of absolute coordinates�h]�h9� Handling of absolute coordinates�����}�(hj  hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj  hhhh,hK!ubhE)��}�(hX>  In most use-cases, absolute input devices are mapped to a single screen. For
direct input devices such as touchscreens the aspect ratio of the screen and
the device match. Mapping the input device position to the output position is
thus a simple mapping between two coordinates. libinput provides the API for
this with�h]�h9X>  In most use-cases, absolute input devices are mapped to a single screen. For
direct input devices such as touchscreens the aspect ratio of the screen and
the device match. Mapping the input device position to the output position is
thus a simple mapping between two coordinates. libinput provides the API for
this with�����}�(hj  hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK#hj  hhubhf)��}�(hhh]�(hk)��}�(h�J**libinput_event_pointer_get_absolute_x_transformed()** for pointer events�h]�hE)��}�(hj/  h]�(h �strong���)��}�(h�7**libinput_event_pointer_get_absolute_x_transformed()**�h]�h9�3libinput_event_pointer_get_absolute_x_transformed()�����}�(h�3libinput_event_pointer_get_absolute_x_transformed()�hj6  ubah}�(h]�h]�h]�h]�h]�uhj4  hj1  ubh9� for pointer events�����}�(h� for pointer events�hj1  ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK)hj-  ubah}�(h]�h]�h]�h]�h]�uhhjhj*  hhhh,hNubhk)��}�(h�>**libinput_event_touch_get_x_transformed()** for touch events
�h]�hE)��}�(h�=**libinput_event_touch_get_x_transformed()** for touch events�h]�(j5  )��}�(h�,**libinput_event_touch_get_x_transformed()**�h]�h9�(libinput_event_touch_get_x_transformed()�����}�(h�(libinput_event_touch_get_x_transformed()�hj^  ubah}�(h]�h]�h]�h]�h]�uhj4  hjZ  ubh9� for touch events�����}�(h� for touch events�hjZ  ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK*hjV  ubah}�(h]�h]�h]�h]�h]�uhhjhj*  hhhh,hNubeh}�(h]�h]�h]�h]�h]�h�h�uhhehh,hK)hj  hhubhE)��}�(hX4  libinput's API only provides the call to map into a single coordinate range.
If the coordinate range has an offset, the compositor is responsible for
applying that offset after the mapping. For example, if the device is mapped
to the right of two outputs, add the output offset to the transformed
coordinate.�h]�h9X6  libinput’s API only provides the call to map into a single coordinate range.
If the coordinate range has an offset, the compositor is responsible for
applying that offset after the mapping. For example, if the device is mapped
to the right of two outputs, add the output offset to the transformed
coordinate.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK,hj  hhubh)��}�(h�.. _absolute_axes_nores:�h]�h}�(h]�h]�h]�h]�h]�h*�absolute-axes-nores�uhhhK7hj  hhhh,ubeh}�(h]�(� handling-of-absolute-coordinates�j
  eh]�h]�(� handling of absolute coordinates��absolute_axes_handling�eh]�h]�uhh-hh/hhhh,hK!�expect_referenced_by_name�}�j�  j   s�expect_referenced_by_id�}�j
  j   subh.)��}�(hhh]�(h3)��}�(h�Devices without x/y resolution�h]�h9�Devices without x/y resolution�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hK6ubhE)��}�(hX  An absolute device that does not provide a valid resolution is considered
buggy and must be fixed in the kernel. Some touchpad devices do not
provide resolution, those devices are correctly handled within libinput
(touchpads are not absolute devices, as mentioned above).�h]�h9X  An absolute device that does not provide a valid resolution is considered
buggy and must be fixed in the kernel. Some touchpad devices do not
provide resolution, those devices are correctly handled within libinput
(touchpads are not absolute devices, as mentioned above).�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK8hj�  hhubh)��}�(h�.. _calibration:�h]�h}�(h]�h]�h]�h]�h]�h*�calibration�uhhhKBhj�  hhhh,ubeh}�(h]�(�devices-without-x-y-resolution�j�  eh]�h]�(�devices without x/y resolution��absolute_axes_nores�eh]�h]�uhh-hh/hhhh,hK6j�  }�j�  j�  sj�  }�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�Calibration of absolute devices�h]�h9�Calibration of absolute devices�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hKAubhE)��}�(h��Absolute devices may require calibration to map precisely into the output
range required. This is done by setting a transformation matrix, see
**libinput_device_config_calibration_set_matrix()** which is applied to
each input coordinate.�h]�(h9��Absolute devices may require calibration to map precisely into the output
range required. This is done by setting a transformation matrix, see
�����}�(h��Absolute devices may require calibration to map precisely into the output
range required. This is done by setting a transformation matrix, see
�hj�  hhhNhNubj5  )��}�(h�3**libinput_device_config_calibration_set_matrix()**�h]�h9�/libinput_device_config_calibration_set_matrix()�����}�(h�/libinput_device_config_calibration_set_matrix()�hj�  ubah}�(h]�h]�h]�h]�h]�uhj4  hj�  ubh9�+ which is applied to
each input coordinate.�����}�(h�+ which is applied to
each input coordinate.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKChj�  hhubh �
math_block���)��}�(h��\begin{pmatrix}
 cos\theta & -sin\theta & xoff \\
 sin\theta &  cos\theta & yoff \\
 0   & 0    & 1
\end{pmatrix} \begin{pmatrix}
x \\ y \\ 1
\end{pmatrix}

�h]�h9��\begin{pmatrix}
 cos\theta & -sin\theta & xoff \\
 sin\theta &  cos\theta & yoff \\
 0   & 0    & 1
\end{pmatrix} \begin{pmatrix}
x \\ y \\ 1
\end{pmatrix}

�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]��docname�hnumber�N�label�N�nowrap���	xml:space��preserve�uhj  hh,hKHhj�  hhubhE)��}�(hX5  :math:`\theta` is the rotation angle. The offsets :math:`xoff` and :math:`yoff` are
specified in device dimensions, i.e. a value of 1 equals one device width or
height. Note that rotation applies to the device's origin, rotation usually
requires an offset to move the coordinates back into the original range.�h]�(h �math���)��}�(h�:math:`\theta`�h]�h9�\theta�����}�(h�\theta�hj/  ubah}�(h]�h]�h]�h]�h]�uhj-  hj)  ubh9�$ is the rotation angle. The offsets �����}�(h�$ is the rotation angle. The offsets �hj)  hhhNhNubj.  )��}�(h�:math:`xoff`�h]�h9�xoff�����}�(h�xoff�hjC  ubah}�(h]�h]�h]�h]�h]�uhj-  hj)  ubh9� and �����}�(h� and �hj)  hhhNhNubj.  )��}�(h�:math:`yoff`�h]�h9�yoff�����}�(h�yoff�hjW  ubah}�(h]�h]�h]�h]�h]�uhj-  hj)  ubh9�� are
specified in device dimensions, i.e. a value of 1 equals one device width or
height. Note that rotation applies to the device’s origin, rotation usually
requires an offset to move the coordinates back into the original range.�����}�(h�� are
specified in device dimensions, i.e. a value of 1 equals one device width or
height. Note that rotation applies to the device's origin, rotation usually
requires an offset to move the coordinates back into the original range.�hj)  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKQhj�  hhubhE)��}�(h�The most common matrices are:�h]�h9�The most common matrices are:�����}�(hjs  hjq  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKVhj�  hhubhf)��}�(hhh]�(hk)��}�(h��90 degree clockwise:
   .. math::
       \begin{pmatrix}
        0 & -1 & 1 \\
        1 & 0 & 0 \\
        0 & 0 & 1
       \end{pmatrix}�h]�h �definition_list���)��}�(hhh]�h �definition_list_item���)��}�(h�x90 degree clockwise:
.. math::
    \begin{pmatrix}
     0 & -1 & 1 \\
     1 & 0 & 0 \\
     0 & 0 & 1
    \end{pmatrix}�h]�(h �term���)��}�(h�90 degree clockwise:�h]�h9�90 degree clockwise:�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hh,hK]hj�  ubh �
definition���)��}�(hhh]�j  )��}�(h�G\begin{pmatrix}
 0 & -1 & 1 \\
 1 & 0 & 0 \\
 0 & 0 & 1
\end{pmatrix}

�h]�h9�G\begin{pmatrix}
 0 & -1 & 1 \\
 1 & 0 & 0 \\
 0 & 0 & 1
\end{pmatrix}

�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��docname�hnumber�N�label�N�nowrap��j'  j(  uhj  hh,hKYhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubeh}�(h]�h]�h]�h]�h]�uhj�  hh,hK]hj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhhjhj  hhhNhNubhk)��}�(h��180 degree clockwise:
   .. math::
       \begin{pmatrix}
        -1 & 0 & 1 \\
        0 & -1 & 1 \\
        0 & 0 & 1
       \end{pmatrix}�h]�j�  )��}�(hhh]�j�  )��}�(h�z180 degree clockwise:
.. math::
    \begin{pmatrix}
     -1 & 0 & 1 \\
     0 & -1 & 1 \\
     0 & 0 & 1
    \end{pmatrix}�h]�(j�  )��}�(h�180 degree clockwise:�h]�h9�180 degree clockwise:�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hh,hKdhj�  ubj�  )��}�(hhh]�j  )��}�(h�H\begin{pmatrix}
 -1 & 0 & 1 \\
 0 & -1 & 1 \\
 0 & 0 & 1
\end{pmatrix}

�h]�h9�H\begin{pmatrix}
 -1 & 0 & 1 \\
 0 & -1 & 1 \\
 0 & 0 & 1
\end{pmatrix}

�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��docname�hnumber�N�label�N�nowrap��j'  j(  uhj  hh,hK`hj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubeh}�(h]�h]�h]�h]�h]�uhj�  hh,hKdhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhhjhj  hhhNhNubhk)��}�(h��270 degree clockwise:
   .. math::
       \begin{pmatrix}
        0 & 1 & 0 \\
        -1 & 0 & 1 \\
        0 & 0 & 1
       \end{pmatrix}�h]�j�  )��}�(hhh]�j�  )��}�(h�y270 degree clockwise:
.. math::
    \begin{pmatrix}
     0 & 1 & 0 \\
     -1 & 0 & 1 \\
     0 & 0 & 1
    \end{pmatrix}�h]�(j�  )��}�(h�270 degree clockwise:�h]�h9�270 degree clockwise:�����}�(hj#  hj!  ubah}�(h]�h]�h]�h]�h]�uhj�  hh,hKkhj  ubj�  )��}�(hhh]�j  )��}�(h�G\begin{pmatrix}
 0 & 1 & 0 \\
 -1 & 0 & 1 \\
 0 & 0 & 1
\end{pmatrix}

�h]�h9�G\begin{pmatrix}
 0 & 1 & 0 \\
 -1 & 0 & 1 \\
 0 & 0 & 1
\end{pmatrix}

�����}�(hhhj2  ubah}�(h]�h]�h]�h]�h]��docname�hnumber�N�label�N�nowrap��j'  j(  uhj  hh,hKghj/  ubah}�(h]�h]�h]�h]�h]�uhj�  hj  ubeh}�(h]�h]�h]�h]�h]�uhj�  hh,hKkhj  ubah}�(h]�h]�h]�h]�h]�uhj�  hj  ubah}�(h]�h]�h]�h]�h]�uhhjhj  hhhNhNubhk)��}�(h��reflection along y axis:
   .. math::
       \begin{pmatrix}
        -1 & 0 & 1 \\
        1 & 0 & 0 \\
        0 & 0 & 1
       \end{pmatrix}
�h]�j�  )��}�(hhh]�j�  )��}�(h�}reflection along y axis:
.. math::
    \begin{pmatrix}
     -1 & 0 & 1 \\
     1 & 0 & 0 \\
     0 & 0 & 1
    \end{pmatrix}
�h]�(j�  )��}�(h�reflection along y axis:�h]�h9�reflection along y axis:�����}�(hji  hjg  ubah}�(h]�h]�h]�h]�h]�uhj�  hh,hKshjc  ubj�  )��}�(hhh]�j  )��}�(h�G\begin{pmatrix}
 -1 & 0 & 1 \\
 1 & 0 & 0 \\
 0 & 0 & 1
\end{pmatrix}

�h]�h9�G\begin{pmatrix}
 -1 & 0 & 1 \\
 1 & 0 & 0 \\
 0 & 0 & 1
\end{pmatrix}

�����}�(hhhjx  ubah}�(h]�h]�h]�h]�h]��docname�hnumber�N�label�N�nowrap��j'  j(  uhj  hh,hKnhju  ubah}�(h]�h]�h]�h]�h]�uhj�  hjc  ubeh}�(h]�h]�h]�h]�h]�uhj�  hh,hKshj`  ubah}�(h]�h]�h]�h]�h]�uhj�  hj\  ubah}�(h]�h]�h]�h]�h]�uhhjhj  hhhNhNubeh}�(h]�h]�h]�h]�h]�h�h�uhhehh,hKXhj�  hhubhE)��}�(hX  See Wikipedia's
`Transformation Matrix article <http://en.wikipedia.org/wiki/Transformation_matrix>`_
for more information on the matrix maths. See
**libinput_device_config_calibration_get_default_matrix()** for how these
matrices must be supplied to libinput.�h]�(h9�See Wikipedia’s
�����}�(h�See Wikipedia's
�hj�  hhhNhNubh �	reference���)��}�(h�U`Transformation Matrix article <http://en.wikipedia.org/wiki/Transformation_matrix>`_�h]�h9�Transformation Matrix article�����}�(h�Transformation Matrix article�hj�  ubah}�(h]�h]�h]�h]�h]��name��Transformation Matrix article��refuri��2http://en.wikipedia.org/wiki/Transformation_matrix�uhj�  hj�  ubh)��}�(h�5 <http://en.wikipedia.org/wiki/Transformation_matrix>�h]�h}�(h]��transformation-matrix-article�ah]�h]��transformation matrix article�ah]�h]��refuri�j�  uhh�
referenced�Khj�  ubh9�/
for more information on the matrix maths. See
�����}�(h�/
for more information on the matrix maths. See
�hj�  hhhNhNubj5  )��}�(h�;**libinput_device_config_calibration_get_default_matrix()**�h]�h9�7libinput_device_config_calibration_get_default_matrix()�����}�(h�7libinput_device_config_calibration_get_default_matrix()�hj�  ubah}�(h]�h]�h]�h]�h]�uhj4  hj�  ubh9�5 for how these
matrices must be supplied to libinput.�����}�(h�5 for how these
matrices must be supplied to libinput.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKuhj�  hhubhE)��}�(h��Once applied, any x and y axis value has the calibration applied before it
is made available to the caller. libinput does not provide access to the
raw coordinates before the calibration is applied.�h]�h9��Once applied, any x and y axis value has the calibration applied before it
is made available to the caller. libinput does not provide access to the
raw coordinates before the calibration is applied.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK{hj�  hhubh)��}�(h�.. _absolute_axes_nonorm:�h]�h}�(h]�h]�h]�h]�h]�h*�absolute-axes-nonorm�uhhhK�hj�  hhhh,ubeh}�(h]�(�calibration-of-absolute-devices�j�  eh]�h]�(�calibration of absolute devices��calibration�eh]�h]�uhh-hh/hhhh,hKAj�  }�j  j�  sj�  }�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�&Why x/y coordinates are not normalized�h]�h9�&Why x/y coordinates are not normalized�����}�(hj  hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj  hhhh,hK�ubhE)��}�(hX�  x/y are not given in :ref:`normalized coordinates <motion_normalization>`
([0..1]) for one simple reason: the aspect ratio of virtually all current
devices is something other than 1:1. A normalized axes thus is only useful to
determine that the stylus is e.g. at 78% from the left, 34% from the top of
the device. Without knowing the per-axis resolution, these numbers are
meaningless. Worse, calculation based on previous coordinates is simply wrong:
a movement from 0/0 to 50%/50% is not a 45-degree line.�h]�(h9�x/y are not given in �����}�(h�x/y are not given in �hj(  hhhNhNubh�)��}�(h�4:ref:`normalized coordinates <motion_normalization>`�h]�h�)��}�(h�-normalized coordinates <motion_normalization>�h]�h9�normalized coordinates�����}�(hhhj5  ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hj1  ubah}�(h]�h]�h]�h]�h]��refdoc�h	refdomain�j@  �reftype��ref��refexplicit���refwarn��hȌmotion_normalization�uhh�hh,hK�hj(  ubh9X�  
([0..1]) for one simple reason: the aspect ratio of virtually all current
devices is something other than 1:1. A normalized axes thus is only useful to
determine that the stylus is e.g. at 78% from the left, 34% from the top of
the device. Without knowing the per-axis resolution, these numbers are
meaningless. Worse, calculation based on previous coordinates is simply wrong:
a movement from 0/0 to 50%/50% is not a 45-degree line.�����}�(hX�  
([0..1]) for one simple reason: the aspect ratio of virtually all current
devices is something other than 1:1. A normalized axes thus is only useful to
determine that the stylus is e.g. at 78% from the left, 34% from the top of
the device. Without knowing the per-axis resolution, these numbers are
meaningless. Worse, calculation based on previous coordinates is simply wrong:
a movement from 0/0 to 50%/50% is not a 45-degree line.�hj(  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj  hhubhE)��}�(h��This could be alleviated by providing resolution and information about the
aspect ratio to the caller. Which shifts processing and likely errors into the
caller for little benefit. Providing the x/y axes in mm from the outset
removes these errors.�h]�h9��This could be alleviated by providing resolution and information about the
aspect ratio to the caller. Which shifts processing and likely errors into the
caller for little benefit. Providing the x/y axes in mm from the outset
removes these errors.�����}�(hj_  hj]  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj  hhubeh}�(h]�(�&why-x-y-coordinates-are-not-normalized�j  eh]�h]�(�&why x/y coordinates are not normalized��absolute_axes_nonorm�eh]�h]�uhh-hh/hhhh,hK�j�  }�jq  j  sj�  }�j  j  subeh}�(h]�(h+�id1�eh]�h]�(�absolute axes��absolute_axes�eh]�h]�uhh-hhhhhh,hKj�  }�j|  h sj�  }�h+h subeh}�(h]�h]�h]�h]�h]��source�h,uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h2N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`94b5be4`�h]�j�  )��}�(h�git commit 94b5be4�h]�h9�git commit 94b5be4�����}�(h�94b5be4�hj�  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/94b5be4�uhj�  hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhj�  h�<rst_prolog>�hKhhub�git_version_full�j�  )��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f8ef17fd830>`

�h]�j�  )��}�(h�<git commit <function get_git_version_full at 0x7f8ef17fd830>�h]�h9�<git commit <function get_git_version_full at 0x7f8ef17fd830>�����}�(h�1<function get_git_version_full at 0x7f8ef17fd830>�hj   ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f8ef17fd830>�uhj�  hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhj�  hj�  hKhhubu�substitution_names�}�(�git_version�j�  �git_version_full�j�  u�refnames�}��refids�}�(h+]�h aj
  ]�j   aj�  ]�j�  aj�  ]�j�  aj  ]�j  au�nameids�}�(j|  h+j{  jx  j�  j
  j�  j�  j�  j�  j�  j�  j  j�  j  j  j�  j�  jq  j  jp  jm  u�	nametypes�}�(j|  �j{  Nj�  �j�  Nj�  �j�  Nj  �j  Nj�  �jq  �jp  Nuh}�(h+h/jx  h/j
  j  j�  j  j�  j�  j�  j�  j�  j�  j  j�  j�  j�  j  j  jm  j  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�hE)��}�(hhh]�h9�3Hyperlink target "absolute-axes" is not referenced.�����}�(hhhjF  ubah}�(h]�h]�h]�h]�h]�uhhDhjC  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h,�line�KuhjA  ubjB  )��}�(hhh]�hE)��}�(hhh]�h9�<Hyperlink target "absolute-axes-handling" is not referenced.�����}�(hhhja  ubah}�(h]�h]�h]�h]�h]�uhhDhj^  ubah}�(h]�h]�h]�h]�h]��level�K�type�j[  �source�h,�line�K"uhjA  ubjB  )��}�(hhh]�hE)��}�(hhh]�h9�9Hyperlink target "absolute-axes-nores" is not referenced.�����}�(hhhj{  ubah}�(h]�h]�h]�h]�h]�uhhDhjx  ubah}�(h]�h]�h]�h]�h]��level�K�type�j[  �source�h,�line�K7uhjA  ubjB  )��}�(hhh]�hE)��}�(hhh]�h9�1Hyperlink target "calibration" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j[  �source�h,�line�KBuhjA  ubjB  )��}�(hhh]�hE)��}�(hhh]�h9�:Hyperlink target "absolute-axes-nonorm" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j[  �source�h,�line�K�uhjA  ube�transformer�N�
decoration�Nhhub.